# LimeSurvey RememberMe plugin

LimeSurvey plugin that adds a cookie to remember logged-in users

## Setup

```bash
composer install --no-dev
```
